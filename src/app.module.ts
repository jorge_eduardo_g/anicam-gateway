import { HttpModule, HttpService, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './product/product.module';
import { ShopModule } from './shop/shop.module';
import { FeaturesModule } from './features/features.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ConfigService } from './config/config.service';
import { ConfigKeys } from './config/config.keys';
import { MongooseModule } from '@nestjs/mongoose';
import { AnicamModule } from './anicam/anicam.module';

const path : any = process.cwd() + '/assets';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forRoot(new ConfigService().get(ConfigKeys.DBCONNECTION),{
      useNewUrlParser:true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    }),
    ProductModule, ShopModule, FeaturesModule,
    ServeStaticModule.forRoot({
      rootPath: path,   // <-- path to the static files
    }),
    AnicamModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
