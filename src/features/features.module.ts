import { HttpModule, Module } from '@nestjs/common';
import { FeaturesController } from './features.controller';
import { FeaturesService } from './features.service';

@Module({
  imports: [HttpModule],
  controllers: [FeaturesController],
  providers: [FeaturesService]
})
export class FeaturesModule {}
