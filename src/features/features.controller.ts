import { Body, Controller, HttpStatus, Logger, Post, Req, Res } from '@nestjs/common';
import { FeaturesService } from './features.service';

@Controller('features')
export class FeaturesController {

  // Create a logger instance
  private logger = new Logger('FeaturesController');
  private readonly startTs : any;

  constructor(
    private featuresService: FeaturesService,
  ) {}

  @Post('/external/get/simplifiedAction')
  async simplifiedAction(@Res() res, @Req() req, @Body() data: any){
    this.logger.log('Getting simplified actions ... '); // Log something on every call
    const result = await this.featuresService.simplifiedAction(data);
    return res.status(HttpStatus.NOT_FOUND).json(result);
  }
}
