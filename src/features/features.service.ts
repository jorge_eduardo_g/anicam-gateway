import { HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigKeys } from '../config/config.keys';
import { map } from 'rxjs/operators';
import { ConfigService } from '../config/config.service';
import { Observable } from 'rxjs';
import * as fs from 'fs';

@Injectable()
export class FeaturesService {

  private configService : ConfigService;

  constructor(private http: HttpService) {
    this.configService = new ConfigService();
  }

  private authAuropaq(): Observable<any> {
    return this.http.post(`${this.configService.get(ConfigKeys.SERVICEAUROPAQ)}/auth`, {
      Email: this.configService.get(ConfigKeys.AUROPAQEMAIL),
      Password: this.configService.get(ConfigKeys.AUROPAQPASSWORD),
      RememberMe: false
    }).pipe(
      map((res: any) => {
        return res.data;
      })
    );
  }

  private getDocumentAuroPaq(token,params): Observable<any> {
    return this.http.post(`${this.configService.get(ConfigKeys.SERVICEAUROPAQ)}/Liquidaciones/GetTransportDocument`, {
      // NumberGuide: "CO042754003",
      NumberGuide: params.GuideNumber.toString(),
      AirwaybillNo: params.AirwaybillNumber.toString(),
      DocumentType: parseInt(params.DocumentType)
    }, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).pipe(
      map((res: any) => {
        return res.data;
      })
    );
  }

  public async simplifiedAction(params): Promise<any> {

    const token = await this.authAuropaq().toPromise()
      .then(res => {
        return res.token;
      });

    const document = await this.getDocumentAuroPaq(token,params).toPromise()
      .then(res => {
        return res;
      });

    if(document.DataSingle) {
      // fs.writeFileSync('any.pdf', document.DataSingle);
      const data = Buffer.from(document.DataSingle, 'base64');
      fs.writeFile(`./assets/files/simplified-actions/SA-${params.GuideNumber.toString()}.pdf`, data, (err) => {
        if (err)
          console.log(err);
      });
    }

    return {
      statusCode: HttpStatus.OK,
      message: document.Message,
      path: `${this.configService.get(ConfigKeys.DOWNLOADPATH)}/files/simplified-actions/SA-${params.GuideNumber.toString()}.pdf`
    };

  }

}
