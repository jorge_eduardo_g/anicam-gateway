import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigKeys } from './config/config.keys';
import { ConfigService } from './config/config.service';
import * as fs from "fs";
import {ExpressAdapter} from "@nestjs/platform-express";
import * as http from "http";
import * as https from "https";
import * as express from 'express';

const configService = new ConfigService();
const httpsOptions = {
  key: fs.readFileSync('ssl/private.key'),
  cert: fs.readFileSync('ssl/assets-certificate.crt'),
  ca: fs.readFileSync('ssl/ca-bundle.crt'),
};

const server = express();

async function bootstrap() {
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(server));

  const allowList = configService.get(ConfigKeys.ORIGINHOST)
  const corsOptionsDelegate = function (req, callback) {
    let corsOptions;
    if (allowList.indexOf(req) !== (-1)) {
      corsOptions = req  // reflect (enable) the requested origin in the CORS response
    } else {
      corsOptions = false  // disable CORS for this request
    }
    callback(null, corsOptions) // callback expects two parameters: error and options
  }

  app.enableCors({
    origin: corsOptionsDelegate,
    credentials: true
  });
  app.setGlobalPrefix(configService.get(ConfigKeys.APIPREFIX));
  await app.init();
  http.createServer(server).listen(parseInt(configService.get(ConfigKeys.APPPORT)));
  https.createServer(httpsOptions, server).listen(443);
  //await app.listen(parseInt(configService.get(ConfigKeys.APPPORT)));
}
bootstrap();
