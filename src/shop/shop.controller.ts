import { Body, Controller, Get, HttpStatus, Logger, Param, Post, Req, Res } from '@nestjs/common';
import { ShopService } from './shop.service';

@Controller('shop')
export class ShopController {

  // Create a logger instance
  private logger = new Logger('ShopController');
  private readonly startTs : any;

  // Inject the administration service
  constructor(private shopService: ShopService) {
    this.startTs=Date.now();
  }


  @Post('/products')
  async searchProducts(@Res() res, @Req() req, @Body() data: any){
    this.logger.log('Searching products ... '); // Log something on every call
    const products = await this.shopService.searchProducts(data);
    if((products.statusCode && products.statusCode !== HttpStatus.OK) || !products.items.length) {
      return res.status(HttpStatus.NOT_FOUND).json(products)
    } else {
      return res.status(HttpStatus.OK).json(products)
    }
  }

  @Post('/titan/products')
  async getTitanProducts(@Res() res, @Req() req, @Body() data: any){
    this.logger.log('Getting titan products ... '); // Log something on every call
    const products = await this.shopService.getTitanProducts(data);
    if((products.statusCode && products.statusCode !== HttpStatus.OK) || !products.items.length) {
      return res.status(HttpStatus.NOT_FOUND).json(products)
    } else {
      return res.status(HttpStatus.OK).json(products)
    }
  }

  @Post('/titan/category/products')
  async getTitanProductsCategory(@Res() res, @Req() req, @Body() data: any){
    this.logger.log('Getting titan products by category ... '); // Log something on every call
    const products = await this.shopService.getTitanProductsCategory(data);
    return res.status(HttpStatus.OK).json(products)
  }

  @Get('/titan/categories')
  async getTitanCategories(@Res() res, @Req() req){
    this.logger.log('Getting titan categories ... '); // Log something on every call
    const categories = await this.shopService.getTitanCategories();
    return res.status(HttpStatus.OK).json(categories)
  }

  @Get('/department')
  async getDepartment(@Res() res, @Req() req){
    this.logger.log('Getting departments ... '); // Log something on every call
    const categories = await this.shopService.getDepartment();
    return res.status(HttpStatus.OK).json(categories)
  }

  @Post('/product')
  async getProduct(@Res() res, @Req() req, @Body() data: any){
    this.logger.log('Getting product detail ...'); // Log something on every call
    const product = await this.shopService.getProduct(data);
    if((product.statusCode && product.statusCode !== HttpStatus.OK) || !product.name) {
      return res.status(HttpStatus.NOT_FOUND).json(product)
    } else {
      return res.status(HttpStatus.OK).json(product)
    }

  }

}
