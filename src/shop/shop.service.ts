import { HttpException, HttpService, HttpStatus, Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { ConfigKeys } from '../config/config.keys';
import { catchError, map } from 'rxjs/operators';
import { AnicamService } from '../anicam/anicam.service';

@Injectable()
export class ShopService {

  token: any;
  constructor(private http: HttpService,
              private readonly configService : ConfigService,
              private anicamService : AnicamService) {  }

  // Format items
  public formatItems(items): any {
    return items.map(item => {
      return {
        attributes: [],
        availability: 'in-stock',
        badges: ['new'],
        brand: {
          id: 1,
          image: '',
          name: item.brand,
          slug: item.brand
        },
        categories: [],
        compareAtPrice: item.high_price,
        customFields: {},
        id: item.code,
        images: [item.large_image],
        name: item.title,
        price: item.list_price,
        rating: item.rating,
        reviews: 50,
        sku: item.code,
        slug: item.title
      }});
  }

  // Call microservice to search products
  public async searchProducts(body): Promise<any> {

    // eslint-disable-next-line @typescript-eslint/camelcase
    body.market_place = 'amazon';
    body.condition = 'new';
    body.prime = true;
    body.keywords = body.category;
    if(!body.page) {
      body.page = 1;
    }
    if(!body.titan) {
      body.titan = true;
    }

    let amzProducts : any = await this.http.post(`${this.configService.get(ConfigKeys.PRIMARYSERVICE)}/search/products/proxy`, body)
      .toPromise()
      .then( async res => {
      return res.data;
    });

    const titanProducts = await this.getTitanProducts({
      idCategory: -1, keyword: body.keywords
    });

    let items = titanProducts.items;
    const counters = {
      limit: titanProducts.limit,
      pages: titanProducts.pages > 0 ? titanProducts.pages : body.page,
      to: titanProducts.limit,
      total: titanProducts.total > 0 ? titanProducts.total * titanProducts.limit : titanProducts.limit
    }

    if(amzProducts?.data) {
      counters.limit += amzProducts.total_results;
      counters.pages += amzProducts.total_pages;
      counters.to += amzProducts.total_results;
      counters.total += amzProducts.total_pages * amzProducts.total_results;
      amzProducts = amzProducts.data.map(item => {
        return {
          attributes: [],
          availability: 'in-stock',
          badges: ['new'],
          brand: {
            id: 1,
            image: '',
            name: item.Brand,
            slug: item.Brand
          },
          categories: [],
          compareAtPrice: item.HighPrice,
          customFields: {},
          id: item.Code,
          images: [item.LargeImage],
          name: item.Title,
          price: item.ListPrice,
          rating: item.Rating,
          reviews: 50,
          sku: item.Code,
          slug: item.Title
        }});
      items = [...items, ...amzProducts];
    }

    amzProducts = {
          filterValues: {},
          filters: [],
          from: 1,
          items,
          page: body.page,
          sort: 'default',
          ...counters
        }

    return amzProducts;

  }

  public async getTitanProducts(body): Promise <any> {

    this.token = await this.anicamService.getToken();
    const products = await this.http.get(`${this.configService.get(ConfigKeys.ANICAMSERVICETITAN)}/brands/categories/search?category_id=${body.idCategory}&q=${body.keyword}`, {
      headers: {
        Authorization: `Bearer ${this.token.token}`
      }
    }).toPromise().then( async res => {
      return res.data;
    });


    return {
      filterValues: {},
      filters: [],
      from: 1,
      items: this.formatItems(products.data.items),
      limit: products.paging.limit,
      page: 1,
      pages: parseFloat((products.paging.total/products.paging.limit).toFixed(0)),
      sort: 'default',
      to: products.paging.limit,
      total: products.paging.total,
    };


  }

  public async getTitanProductsCategory(body): Promise <any> {

    this.token = await this.anicamService.getToken();

    const products = await this.http.get(`${this.configService.get(ConfigKeys.ANICAMSERVICETITAN)}/brands/categories/search?category_id=${body.idCategory}`, {
      headers: {
        Authorization: `Bearer ${this.token.token}`
      }
    }).toPromise().then( async res => {
      return res.data;
    });


      return {
        filterValues: {},
        filters: [],
        from: 1,
        items: this.formatItems(products.data.items),
        limit: products.paging.limit,
        page: products.paging.offset,
        pages: parseFloat((products.paging.total/products.paging.limit).toFixed(0)),
        sort: 'default',
        to: products.paging.limit,
        total: products.paging.total,
      };


  }

  // Call microservice to get titan categories
  public async getTitanCategories (): Promise<any> {

    this.token = await this.anicamService.getToken();
    const categories = await this.http.get(`${this.configService.get(ConfigKeys.ANICAMSERVICETITAN)}/brands/categories`, {
      headers: {
          Authorization: `Bearer ${this.token.token}`
      }
    }).toPromise().then( async res => {
      return res.data;
    });

    if(categories.status !== HttpStatus.OK) {
      return new NotFoundException('categories not found').getResponse();
    }


    let parentCategories = categories.data.items.filter(category => category.parent_id === -1 || category.parent_id === 0);
    parentCategories = parentCategories.map(category => {

      const children = categories.data.items.filter(category1 => category1.parent_id === category.id).map(subcategory => {

        const subChildren = categories.data.items.filter(category2 => category2.parent_id === subcategory.id).map(subSubCategory => {
          return {
            children: [],
            customFields: {},
            id: subSubCategory.id,
            image: null,
            items: 0,
            name: subSubCategory.name,
            parents: [
              {
                children: [],
                customFields: {},
                id: 0,
                image: null,
                items: 0,
                name: 'Nuestras Categorias',
                parents: null,
                path: 'Nuestras Categorias',
                slug: '',
                type: 'shop'
              },
              {
                children: [],
                customFields: {},
                id: category.id,
                image: null,
                items: 0,
                name: category.name,
                parents: [],
                path: category.name,
                slug: category.name.replace(/-/g,'').replace(/ /g,'-'),
                type: 'shop'
              },
              {
                children: [],
                customFields: {},
                id: subcategory.id,
                image: null,
                items: 0,
                name: subcategory.name,
                parents: [],
                path: `${category.name}/${subcategory.name}`,
                slug: subcategory.name.replace(/-/g,'').replace(/ /g,'-'),
                type: 'shop'
              }],
            path: `${category.name}/${subcategory.name}/${subSubCategory.name}`,
            slug: subSubCategory.name.replace(/-/g,'').replace(/ /g,'-'),
            type: 'shop'
          }
        });

        return {
          children: subChildren,
          customFields: {},
          id: subcategory.id,
          image: null,
          items: subChildren.length,
          name: subcategory.name,
          parents: [{
            children: [],
            customFields: {},
            id: 0,
            image: null,
            items: 0,
            name: 'Nuestras Categorias',
            parents: null,
            path: 'Nuestras Categorias',
            slug: '',
            type: 'shop'
          },
            {
            children: [],
            customFields: {},
            id: category.id,
            image: null,
            items: 0,
            name: category.name,
            parents: [],
            path: category.name,
            slug: category.name.replace(/-/g,'').replace(/ /g,'-'),
            type: 'shop'
          }],
          path: `${category.name}/${subcategory.name}`,
          slug: subcategory.name.replace(/-/g,'').replace(/ /g,'-'),
          type: 'shop'
        }
      });

      return {
        children,
        customFields: {},
        id: category.id,
        image: null,
        items: children.length,
        name: category.name,
        parents: [{
          children: [],
          customFields: {},
          id: 0,
          image: null,
          items: 0,
          name: 'Nuestras Categorias',
          parents: null,
          path: 'Nuestras Categorias',
          slug: '',
          type: 'shop'
        }],
        path: category.name,
        slug: category.name.replace(/-/g,'').replace(/ /g,'-'),
        type: 'shop'
      }
    });

    return parentCategories;
  }

  public async getDepartment (): Promise<any> {

    const baseUrl = '/shop/products';
    const baseUrlTitan = '/shop/titan';

    const ourDepartment = {
      label: 'Nuestras Categorias',
      url: baseUrl,
      menu: {
        type: 'menu',
        items: []
      }
    };

    let categories = await this.getTitanCategories();

    categories = categories.map(category=> {
      let subcategories;
      if (category.children.length) {
        subcategories = category.children.map(subcategory => {
          let subSubcategories;
          if (subcategory.children.length) {
            subSubcategories = subcategory.children.map(subSubcategory => {
              return {
                label: subSubcategory.name,
                url: `${baseUrlTitan}/${subSubcategory.id}/`,
              };
            });
          }
          return {
            label: subcategory.name,
            url: `${baseUrlTitan}/${subcategory.id}/`,
            items: subSubcategories
          };
        })
      }
      return {
        label: category.name,
        url: `${baseUrlTitan}/${category.id}/`,
        items: subcategories
      }
    });

    ourDepartment.menu.items = categories;

    const departments = [
      {label: 'Power Tools', url: '/shop/products/Power Tools', menu: {
          type: 'megamenu',
          size: 'xl',
          image: 'assets/images/megamenu/megamenu-1.jpg',
          columns: [
            {size: 3, items: [
                {label: 'Power Tools', url: '/shop/products/Power Tools', items: [
                    {label: 'Engravers', url: '/shop/products/Engravers'},
                    {label: 'Drills', url: '/shop/products/Drills'},
                    {label: 'Wrenches', url: '/shop/products/Wrenches'},
                    {label: 'Plumbing', url: '/shop/products/Plumbing'},
                    {label: 'Wall Chaser', url: '/shop/products'},
                    {label: 'Pneumatic Tools', url: '/shop/products'},
                    {label: 'Milling Cutters', url: '/shop/products'}
                  ]},
                {label: 'Workbenches', url: '/shop/products'},
                {label: 'Presses', url: '/shop/products'},
                {label: 'Spray Guns', url: '/shop/products'},
                {label: 'Riveters', url: '/shop/products'}
              ]},
            {size: 3, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                  ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                  ]}
              ]},
            {size: 3, items: [
                {label: 'Machine Tools', url: '/shop/products', items: [
                    {label: 'Thread Cutting', url: '/shop/products'},
                    {label: 'Chip Blowers', url: '/shop/products'},
                    {label: 'Sharpening Machines', url: '/shop/products'},
                    {label: 'Pipe Cutters', url: '/shop/products'},
                    {label: 'Slotting machines', url: '/shop/products'},
                    {label: 'Lathes', url: '/shop/products'}
                  ]}
              ]},
            {size: 3, items: [
                {label: 'Instruments', url: '/shop/products', items: [
                    {label: 'Welding Equipment', url: '/shop/products'},
                    {label: 'Power Tools', url: '/shop/products'},
                    {label: 'Hand Tools', url: '/shop/products'},
                    {label: 'Measuring Tool', url: '/shop/products'}
                  ]}
              ]}
          ]
        }},
      {label: 'Hand Tools', url: baseUrl, menu: {
          type: 'megamenu',
          size: 'lg',
          image: 'assets/images/megamenu/megamenu-2.jpg',
          columns: [
            {size: 4, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                  ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                  ]}
              ]},
            {size: 4, items: [
                {label: 'Machine Tools', url: '/shop/products', items: [
                    {label: 'Thread Cutting', url: '/shop/products'},
                    {label: 'Chip Blowers', url: '/shop/products'},
                    {label: 'Sharpening Machines', url: '/shop/products'},
                    {label: 'Pipe Cutters', url: '/shop/products'},
                    {label: 'Slotting machines', url: '/shop/products'},
                    {label: 'Lathes', url: '/shop/products'}
                  ]}
              ]},
            {size: 4, items: [
                {label: 'Instruments', url: '/shop/products', items: [
                    {label: 'Welding Equipment', url: '/shop/products'},
                    {label: 'Power Tools', url: '/shop/products'},
                    {label: 'Hand Tools', url: '/shop/products'},
                    {label: 'Measuring Tool', url: '/shop/products'}
                  ]}
              ]}
          ]
        }},
      {label: 'Machine Tools', url: baseUrl, menu: {
          type: 'megamenu',
          size: 'nl',
          image: 'assets/images/megamenu/megamenu-3.jpg',
          columns: [
            {size: 6, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                  ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                  ]}
              ]},
            {size: 6, items: [
                {label: 'Instruments', url: '/shop/products', items: [
                    {label: 'Welding Equipment', url: '/shop/products'},
                    {label: 'Power Tools', url: '/shop/products'},
                    {label: 'Hand Tools', url: '/shop/products'},
                    {label: 'Measuring Tool', url: '/shop/products'}
                  ]}
              ]}
          ]
        }},
      {label: 'Building Supplies', url: baseUrl, menu: {
          type: 'megamenu',
          size: 'sm',
          columns: [
            {size: 12, items: [
                {label: 'Hand Tools', url: '/shop/products', items: [
                    {label: 'Screwdrivers', url: '/shop/products'},
                    {label: 'Handsaws', url: '/shop/products'},
                    {label: 'Knives', url: '/shop/products'},
                    {label: 'Axes', url: '/shop/products'},
                    {label: 'Multitools', url: '/shop/products'},
                    {label: 'Paint Tools', url: '/shop/products'}
                  ]},
                {label: 'Garden Equipment', url: '/shop/products', items: [
                    {label: 'Motor Pumps', url: '/shop/products'},
                    {label: 'Chainsaws', url: '/shop/products'},
                    {label: 'Electric Saws', url: '/shop/products'},
                    {label: 'Brush Cutters', url: '/shop/products'}
                  ]}
              ]}
          ]
        }},
      {label: 'Electrical', url: baseUrl, menu: {
          type: 'menu',
          items: [
            {label: 'Soldering Equipment', url: '/shop/products', items: [
                {label: 'Soldering Station', url: '/shop/products'},
                {label: 'Soldering Dryers', url: '/shop/products'},
                {label: 'Gas Soldering Iron', url: '/shop/products'},
                {label: 'Electric Soldering Iron', url: '/shop/products'}
              ]},
            {label: 'Light Bulbs', url: '/shop/products'},
            {label: 'Batteries', url: '/shop/products'},
            {label: 'Light Fixtures', url: '/shop/products'},
            {label: 'Warm Floor', url: '/shop/products'},
            {label: 'Generators', url: '/shop/products'},
            {label: 'UPS', url: '/shop/products'}
          ]
        }},
      {label: 'Power Machinery',        url: baseUrl},
      {label: 'Measurement',            url: baseUrl},
      {label: 'Plumbing',               url: baseUrl},
      {label: 'Storage & Organization', url: baseUrl},
      {label: 'Welding & Soldering',    url: baseUrl}
    ]
    return [ourDepartment, ...departments];
  }

  public async getProduct ({id, lang}: any) : Promise<any> {
    if(!id) {
      return new NotFoundException(id).getResponse();
    }
    const language = `${lang}_US`;
    const availability = 'in-stock';

    const uri = encodeURI(`${this.configService.get(ConfigKeys.ANICAMSERVICETITAN)}/brands/items/${id}`);
    console.log(uri);

    this.token = await this.anicamService.getToken();
    let product = await this.http.get(uri, {
      headers: {
        Authorization: `Bearer ${this.token.token}`
      }
    }).toPromise().then( async res => {
      return res.data;
    }).catch(e => {
      return e.response.data;
    });

    if(product.status !== HttpStatus.OK) {
      const body = {
        // eslint-disable-next-line @typescript-eslint/camelcase
        user_id: "6B3E7382-2593-4837-A8A9-2D775642750B1",
        // eslint-disable-next-line @typescript-eslint/camelcase
        market_place: 'amazon',
        code: id,
        language
      }
      console.log(body);
      product = await this.http.post(`${this.configService.get(ConfigKeys.PRIMARYSERVICE)}/search/product/proxy`, body)
        .toPromise().then( async res => {
        return res.data;
      }).catch(e => {
          return new NotFoundException('products not found').getResponse();
      });
    }

    if(product.status !== HttpStatus.OK) {
      return new NotFoundException(product).getResponse();
    }

    if(product.data.Code) {
      const data = product.data;
      product.data = {
        code: data.Code,
        title: data.Title,
        // eslint-disable-next-line @typescript-eslint/camelcase
        title_en: data.Title,
        description: data.Description,
        // eslint-disable-next-line @typescript-eslint/camelcase
        description_en: data.Description,
        descriptionHtml: data.DescriptionHTML,
        color: data.Color,
        brand: data.Brand,
        images: data.Images,
        // eslint-disable-next-line @typescript-eslint/camelcase
        market_place: data.MarketPlace,
        money: data.Money,
        notes: data.Notes,
        prime: data.Prime,
        quantity: data.Quantity,
        // eslint-disable-next-line @typescript-eslint/camelcase
        shipping_allowed: data.Shipping_allowed,
        url: data.Url,
        // eslint-disable-next-line @typescript-eslint/camelcase
        variations_size: data.VariationsSize,
        // eslint-disable-next-line @typescript-eslint/camelcase
        variations_color: data.VariationsColor,
        // eslint-disable-next-line @typescript-eslint/camelcase
        customer_review: data.CustomerReview,
        condition: data.Condition,
        availability: data.Availability,
        // eslint-disable-next-line @typescript-eslint/camelcase
        product_type_name: data.ProductTypeName,
        category: data.Category,
        price: data.Price,
        // eslint-disable-next-line @typescript-eslint/camelcase
        large_image: data.LargeImage,
        // eslint-disable-next-line @typescript-eslint/camelcase
        medium_image: data.MediumImage,
        // eslint-disable-next-line @typescript-eslint/camelcase
        small_image: data.SmallImage,
        // eslint-disable-next-line @typescript-eslint/camelcase
        item_weight: data.Item_weight,
        // eslint-disable-next-line @typescript-eslint/camelcase
        item_length: data.Item_length,
        // eslint-disable-next-line @typescript-eslint/camelcase
        item_width: data.Item_width,
        // eslint-disable-next-line @typescript-eslint/camelcase
        item_height: data.Item_height,
        // eslint-disable-next-line @typescript-eslint/camelcase
        package_weight: data.Package_weight,
        // eslint-disable-next-line @typescript-eslint/camelcase
        package_length: data.Package_length,
        // eslint-disable-next-line @typescript-eslint/camelcase
        package_height: data.Package_height,
        // eslint-disable-next-line @typescript-eslint/camelcase
        package_width: data.Package_width,
        features: data.Features,
        rating: data.Rating
      };
    }

    product = product.data;

    const images = product.images.map((item) => {
      if(item.url) {
        return item.url;
      }
      if(item.URL) {
        return item.URL;
      }
    });

    const attributes = [];
    let specification = [
      {name: 'General', features: [
          /*{name: 'Material', value: 'Aluminium, Plastic'},
          {name: 'Engine Type', value: 'Brushless'},
          {name: 'Battery Voltage', value: '18 V'},
          {name: 'Battery Type', value: 'Li-lon'},
          {name: 'Number of Speeds', value: '2'},
          {name: 'Charge Time', value: '1.08 h'},*/
          {name: 'Brand', value: `${product.brand}`},
          {name: 'Condition', value: `${product.condition}`},
          {name: 'Weight', value: `${product.item_weight} pound(s)`}
        ]},
      {name: 'Dimensions', features: [
          {name: 'Length', value: `${product.item_length} inches`},
          {name: 'Width', value: `${product.item_width} inches`},
          {name: 'Height', value: `${product.item_height} inches`}
        ]}
    ];

    if(lang === 'es') {
      specification = [
        {name: 'General', features: [
            {name: 'Marca', value: `${product.brand}`},
            {name: 'Condición', value: `${product.condition}`},
            {name: 'Peso', value: `${product.item_weight} libra(s)`}
          ]},
        {name: 'Dimensiones', features: [
            {name: 'Largo', value: `${product.item_length} pulgadas`},
            {name: 'Ancho', value: `${product.item_width} pulgadas`},
            {name: 'Alto', value: `${product.item_height} pulgadas`}
          ]}
      ];
    }

    return {
      id,
      name: product.title,
      sku: product.code,
      slug: product.code,
      price: product.price,
      compareAtPrice: product.compareAtPrice || null,
      images: images,
      badges: ['hot'],
      rating: product.rating,
      reviews: product.customer_review,
      availability,
      brand: product.brand || null,
      categories: product.product_type_name.split('|'),
      attributes,
      customFields: {
        itemWeight: product.item_weight,
        itemLength: product.item_length,
        itemWidth: product.item_width,
        itemHeight: product.item_height,
        packageWeight: product.package_weight,
        packageLength: product.package_length,
        packageHeight: product.package_height,
        packageWidth: product.package_width,
      },
      descriptionSp: product.description,
      descriptionEn: product.description_en,
      descriptionHtml: product.descriptionHtml,
      specification,
      features: product.features,
      variationSize: product.variations_size,
      variationColor: product.variations_color
    };

  }
}
