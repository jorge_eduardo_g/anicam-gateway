import { HttpModule, Module } from '@nestjs/common';
import { ShopController } from './shop.controller';
import { ShopService } from './shop.service';
import { ConfigService } from '../config/config.service';
import { AnicamModule } from '../anicam/anicam.module';
import { AnicamService } from '../anicam/anicam.service';

@Module({
  imports: [
    AnicamModule,
    HttpModule,
    ConfigService,
  ],
  controllers: [ShopController],
  providers: [ShopService,ConfigService]
})
export class ShopModule {}
