import { HttpException, HttpService, Injectable } from '@nestjs/common';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from '../config/config.service';
import { ConfigKeys } from '../config/config.keys';

@Injectable()
export class ProductService {

  private configService : ConfigService;

  constructor(private http: HttpService) {
    this.configService = new ConfigService();
  }

  // Call microservice to search products
  public searchProducts(body) {

    // eslint-disable-next-line @typescript-eslint/camelcase
    body.market_place = 'amazon';
    body.condition = 'new';
    body.prime = true;

    return this.http.post(`${this.configService.get(ConfigKeys.PRIMARYSERVICE)}/search/products/proxy`, body).pipe(
      map(response => {
        return response.data
      }),
      catchError( e=> {
        throw new HttpException(e.response.data, e.response.status);
      }),
    );

  }

  public productDetail(asin) {

    const body = {
      // eslint-disable-next-line @typescript-eslint/camelcase
      user_id: "6B3E7382-2593-4837-A8A9-2D775642750B1",
      // eslint-disable-next-line @typescript-eslint/camelcase
      market_place: 'amazon',
      code: asin
    }
    return this.http.post(`${this.configService.get(ConfigKeys.PRIMARYSERVICE)}/search/product/proxy`, body).pipe(
      map(response => {
        return response.data
      }),
      catchError( e=> {
        throw new HttpException(e.response.data, e.response.status);
      }),
    );
  }


}
