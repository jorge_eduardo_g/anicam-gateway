import { Body, Controller, Get, HttpStatus, Logger, Param, Post, Req, Res } from '@nestjs/common';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {

  // Create a logger instance
  private logger = new Logger('ProductController');
  private readonly startTs : any;

  // Inject the administration service
  constructor(private productService: ProductService) {
    this.startTs=Date.now();
  }


  @Post('/search')
  // Define the logic to be executed
  async searchProducts(@Res() res, @Req() req, @Body() data: any){
    this.logger.log('Getting search products ... '); // Log something on every call
    const products = this.productService.searchProducts(data);
    products.subscribe(data => {
      return res.status(HttpStatus.OK).json(data)
    }, err => {
      return res.status(HttpStatus.NOT_FOUND).json(err.response)
    })
  }

  @Get('/detail/:asin')
  // Define the logic to be executed
  async detailProduct(@Res() res, @Req() req, @Param('asin') asin){
    this.logger.log('Getting detail product ... ', asin); // Log something on every call
    const products = this.productService.productDetail(asin);
    products.subscribe(data => {
      return res.status(HttpStatus.OK).json(data)
    }, err => {
      return res.status(HttpStatus.NOT_FOUND).json(err.response)
    })
  }

}
