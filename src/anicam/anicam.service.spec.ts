import { Test, TestingModule } from '@nestjs/testing';
import { AnicamService } from './anicam.service';

describe('AnicamService', () => {
  let service: AnicamService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnicamService],
    }).compile();

    service = module.get<AnicamService>(AnicamService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
