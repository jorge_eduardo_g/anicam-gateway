import { BadRequestException, HttpService, Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { ConfigService } from '../config/config.service';
import { InjectModel } from '@nestjs/mongoose';
import { TokenInterface } from './interface/token.interface';
import { ConfigKeys } from '../config/config.keys';
import * as moment from 'moment';
import 'moment/locale/es';

@Injectable()
export class AnicamService {

  token: any;
  constructor(
    private http: HttpService,
    @InjectModel('Token') private tokenInterface: Model<TokenInterface>,
    private readonly configService : ConfigService
  ) {
  }

  // Validate and get token
  public async getToken(): Promise<any> {
    let token = await this.validateToken({
      username: this.configService.get(ConfigKeys.USERDEFAULT)
    });
    const expires = moment(token.expires);
    if (expires.isBefore(moment())){
      token = await this.generateToken({
        username: this.configService.get(ConfigKeys.USERDEFAULT),
        password: this.configService.get(ConfigKeys.PASSWORDDEFAULT),
        // eslint-disable-next-line @typescript-eslint/camelcase
        grant_type: 'password'
      });
    }
    return token;
  }

  private static serialize(obj) {
    const str = [];
    for (const p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  public async generateToken(params) : Promise<any> {

    try {
      const paramsSerialize = AnicamService.serialize(params);
      const tokenRequest : any = await this.http.post(`${this.configService.get(ConfigKeys.ANICAMSERVICE)}/token`, paramsSerialize, {
        headers: {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }
        }
      }).toPromise().then( async res => {
        return res.data;
      });
      const expires = moment(tokenRequest['.expires']);

      let tokenExists = await this.tokenInterface.findOne({
        user: params.username
      });
      
      if(!tokenExists) {
        tokenExists = await this.tokenInterface.create({
          token: tokenRequest.access_token,
          user: params.username,
          password: params.password,
          expires: expires.format('x'),
          expiresFormat: expires.format('DD/MM/YYYY h:mm a'),
        });
      } else {
        await this.tokenInterface.updateOne({user: params.username},{
          token: tokenRequest.access_token,
          user: params.username,
          password: params.password,
          expires: expires.format('x'),
          expiresFormat: expires.format('DD/MM/YYYY h:mm a'),
        });
        tokenExists = await this.tokenInterface.findOne({
          user: params.username
        });
      }

      return tokenExists;
    } catch (e) {
      return new BadRequestException(e).getResponse();
    }

  }

  public async validateToken(params) : Promise<any> {

    let getToken = await this.tokenInterface.findOne({
      user: params.username
    });

    if(!getToken) {
      getToken = await this.generateToken({
        username: this.configService.get(ConfigKeys.USERDEFAULT),
        password: this.configService.get(ConfigKeys.PASSWORDDEFAULT),
        // eslint-disable-next-line @typescript-eslint/camelcase
        grant_type: 'password'
      });
    }

    return getToken;

  }

  public async quotationProduct({productType, country = this.configService.get(ConfigKeys.QUOTATIONCOUNTRY),
     state = this.configService.get(ConfigKeys.QUOTATIONSTATE),
     city = this.configService.get(ConfigKeys.QUOTATIONCITY),
     user = this.configService.get(ConfigKeys.QUOTATIONUSER), price,
     packageHeight, packageLength, packageWeight, packageWidth }) : Promise <any> {

    this.token = await this.getToken();
    const place = 2;

    const dataTariff = {
      token: this.token.token,
      tariff: productType,
      place,
      country
    };
    const tariff = await this.http.post(`${this.configService.get(ConfigKeys.DEFAULTSERVICE)}/tariffs`, dataTariff)
      .toPromise().then( async res => {
        return res.data;
      }).catch(e => {
        return new NotFoundException(e).getResponse();
      });

    delete dataTariff.country;
    const dataShipping = {
      ...dataTariff,
      tariff: tariff[0].code,
      // eslint-disable-next-line @typescript-eslint/camelcase
      weight_lb: packageWeight,
      // eslint-disable-next-line @typescript-eslint/camelcase
      height_in: packageHeight,
      // eslint-disable-next-line @typescript-eslint/camelcase
      length_in: packageLength,
      // eslint-disable-next-line @typescript-eslint/camelcase
      width_in: packageWidth,
      // eslint-disable-next-line @typescript-eslint/camelcase
      declare_value: price,
      // eslint-disable-next-line @typescript-eslint/camelcase
      delivery_country_locale: country,
      // eslint-disable-next-line @typescript-eslint/camelcase
      delivery_state: state,
      // eslint-disable-next-line @typescript-eslint/camelcase
      delivery_city: city,
      // eslint-disable-next-line @typescript-eslint/camelcase
      user_id: user || this.configService.get(ConfigKeys.USERDEFAULT)
    }

    // console.log(dataShipping);

    return await this.http.post(`${this.configService.get(ConfigKeys.DEFAULTSERVICE)}/rates/shipping`, dataShipping)
      .toPromise().then( async res => {
        return res.data;
      }).catch(e => {
        return new NotFoundException(e).getResponse();
      });

  }


}
