import {Document} from 'mongoose';

export interface TokenInterface extends Document{
  token: string,
  user: string,
  password: string,
  expires?: any,
  expiresFormat?: string,
  createdBy?: number,
  updatedBy?: number,
}
