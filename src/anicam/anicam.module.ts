import { HttpModule, HttpService, Module } from '@nestjs/common';
import { AnicamService } from './anicam.service';
import { AnicamController } from './anicam.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TokenSchema } from './schema/token.schema';
import { ConfigService } from '../config/config.service';

@Module({
  imports: [
    HttpModule,
    ConfigService,
    MongooseModule.forFeature([
      {name: 'Token', schema: TokenSchema},
    ])
  ],
  providers: [AnicamService,ConfigService],
  controllers: [AnicamController],
  exports: [AnicamService]
})
export class AnicamModule {}
