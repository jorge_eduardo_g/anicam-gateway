import { Body, Controller, Get, HttpStatus, Logger, Post, Query, Req, Res } from '@nestjs/common';
import { ShopService } from '../shop/shop.service';
import { AnicamService } from './anicam.service';

@Controller('anicam')
export class AnicamController {

  // Create a logger instance
  private logger = new Logger('AnicamController');
  private readonly startTs : any;

  // Inject the administration service
  constructor(private anicamService: AnicamService) {
    this.startTs=Date.now();
  }

  @Get('/generateToken')
  async generateToken(@Res() res, @Req() req, @Query() query) {
    this.logger.log('Generating token ... '); // Log something on every call
    const token = await this.anicamService.generateToken(query);
    return res.status(HttpStatus.OK).json(token)
  }

  @Get('/validateToken')
  async validateToken(@Res() res, @Req() req, @Query() query) {
    this.logger.log('Validating token ... '); // Log something on every call
    const token = await this.anicamService.validateToken(query);
    return res.status(HttpStatus.OK).json(token)
  }

  @Post('/product/quotation')
  async quotationProduct(@Res() res, @Req() req, @Body() data: any) {
    this.logger.log('Quotation Product ...'); // Log something on every call
    const result = await this.anicamService.quotationProduct(data);
    return res.status(HttpStatus.OK).json(result)
  }

}
