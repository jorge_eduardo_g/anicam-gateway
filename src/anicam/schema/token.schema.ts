import { Schema } from 'mongoose'
import * as moment from 'moment';
import 'moment/locale/es';

export const TokenSchema = new Schema({
  token: String,
  user: String,
  password: String,
  expires: Number,
  expiresFormat: String,
  createdAt: Number,
  createdAtFormat:  { type: String, required: false, default: moment(this.createdAt).format('DD/MM/YYYY h:mm a') },
  createdBy: Number,
  updatedAt: Number,
  updatedBy: Number,
}, {
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
  timestamps: true,
  collection: 'token',
  versionKey: false // You should be aware of the outcome after set to false
});/*

TokenSchema.virtual('createdAtFormated')
  .get (() => {
    console.log(moment('Wed, 22 Dec 2021 21:29:15 GMT'));
    return moment(this.createdAt).format('DD/MM/YYYY h:mm a')
  });*/
