import { Test, TestingModule } from '@nestjs/testing';
import { AnicamController } from './anicam.controller';

describe('AnicamController', () => {
  let controller: AnicamController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnicamController],
    }).compile();

    controller = module.get<AnicamController>(AnicamController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
